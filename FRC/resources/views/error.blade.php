@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header" style="display: flex; justify-content: center; align-items: center">
                        {{ __('Вам відмовлено у доступі') }}
                    </div>

                    <div class="card-body" style="display: flex; justify-content: center; align-items: center">
                        <button onclick="window.location.href ='/FRC/public/warehouses'" style="background-color: crimson; color: mintcream; border-radius: 3px">Повернутись назад</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
