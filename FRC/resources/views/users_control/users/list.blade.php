@extends('users_control.layout')

@section('content')

    <div class="container"  style="display: flex; justify-content: center; align-items: center">
    <p id="H_style1">Список користувачів</p>
    </div>
    <div class="container"  style="display: flex; justify-content: center; align-items: center">
    <table id="table_style">
        <th id="td2_style">id користувача</th>
        <th id="td2_style">Ім'я користувача</th>
        <th id="td2_style">Запис створено</th>
        <th id="td2_style">Запис оновлено</th>
        <th id="td2_style">Роль</th>
        <th id="td2_style">Опції</th>
        @foreach ($users as $u)
            <tr>
                <td id="td1_style">{{ $u->id }}</td>
                <td id="td1_style">{{ $u->name }}</td>
                <td id="td1_style">{{ $u->created_at }}</td>
                <td id="td1_style">{{ $u->updated_at }}</td>
                <td id="td1_style">{{ $u->role }}</td>
                <td id="td1_style">
                    <a href="/FRC/public/operations/{{ $u->id }}/edit" id="href_style">редагувати</a>
                    <form style="margin:  5px;" action="/FRC/public/operations/{{ $u->id }}"method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button id="select_style">Видалити</button>

                    </form>
                </td>
            </tr>
        @endforeach
    </table>
    </div>

@endsection
