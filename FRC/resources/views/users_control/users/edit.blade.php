@extends('users_control.layout')
<style type="text/css">

</style>
<style type="text/css">
    label {
        min-width: 150px;
        display: inline-block;
    }
</style>
@section('content')
    <div class="container"  style="display: flex; justify-content: center; align-items: center">
    <form action="/FRC/public/operations/{{ $users->id }}" method="POST">
        {{ method_field('PUT') }}
        {{ csrf_field() }}
        <div class="container"  style="display: flex; justify-content: center; align-items: center">
            <p id="text_style1">Користувач: {{ $users->name }}</p>
        </div>
        <label id="span_style">Роль користувача: </label>
        <select name="role" id="select_style">
            @foreach($role as $role_type => $role_title)
                <option value="{{ $role_type }}">
                    {{ $role_title }}
                </option>
            @endforeach
        </select>
        <br/>
        <br/>
        <div class="container"  style="display: flex; justify-content: center; align-items: center">
        <input type="submit" value="Зберегти" id="input_style2">
        </div>
    </form>
    </div>
@endsection
