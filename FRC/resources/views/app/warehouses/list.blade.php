@extends('app.layouts_frc.layout')

<?php
$model_warehouse = new App\Models\Warehouses();
$c_id = $model_warehouse->getCargoID();

?>

@section('content')
    <div class="container"  style="display: flex; justify-content: center; align-items: center">
    <button onclick="window.location.href ='/FRC/public/admin/warehouses'" id="button_style1">Внести зміни</button>
    </div>

    <div class="container"  style="display: flex; justify-content: center; align-items: center">
    <form method="get" action="/FRC/public/warehouses">
        <span id="span_style">склад :</span>
        <select name="warehouse_id" id="select_style">
            <option value="0">всі</option>
            @foreach($warehouse_id as $w_id => $w_title)
                <option value="{{ $w_id }}"
                    {{ ( $w_id == $warehouse_id_selected ) ? 'selected' : '' }}>
                    {{ $w_title }}
                </option>
            @endforeach
        </select>

        <span id="span_style">id :</span>
        <select name="cargo_id" id="select_style">
            <option value="0">всі</option>
            @foreach($c_id as $cargo_id)
                <option value="{{ $cargo_id->cargo_id }}"
                    {{ ( $cargo_id->cargo_id == $cargo_id_selected ) ? 'selected' : '' }}>
                    {{ $cargo_id->cargo_id }}
                </option>
            @endforeach
        </select>

        <p><input type="submit" value="виконати" id="submit_style"/></p>
        <div class="container"  style="display: flex; justify-content: center; align-items: center">
        <p id="text_style1">Список вантажу: </p>
        </div>
    </form>
    </div>

    <div class="container"  style="display: flex; justify-content: center; align-items: center">
    <table id="table_style">

        <th id="th_style">Назва вантажу</th>
        <th id="th_style">Ціна вантажу</th>
        <th id="th_style">id вантажу</th>
        <th id="th_style">Назва складу</th>

        @foreach ($warehouses as $warehouse)
            <tr>
                <td id="td_style" >
                    <a href="/FRC/public/warehouses/{{ $warehouse->cargo_id }}" id="href_style">
                        {{ $warehouse->cargo_name}}
                    </a>
                </td>
                <td id="td_style">{{ $warehouse->cargo_price }}</td>
                <td id="td_style">{{ $warehouse->cargo_id }}</td>
                <td id="td_style">{{ $warehouse->warehouse_name }}</td>
            </tr>
        @endforeach
    </table>
    </div>


@endsection
