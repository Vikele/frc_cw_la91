@extends('app.layouts_frc.layout')


@section('content')
    <div class="container"  style="display: flex; justify-content: center; align-items: center">
    <p id="text_style1">Інформація про вантаж: {{ $warehouse->cargo_id }}</p>
    </div>
    <div class="container"  style="display: flex; justify-content: center; align-items: center">
    <table id="table_style">
        <th id="td2_style">Назва вантажу</th>
        <th id="td2_style">Ціна вантажу</th>
        <th id="td2_style">Місце доставки</th>
        <th id="td2_style">id складу</th>
        <tr>
            <td id="td2_style"> {{ $warehouse->cargo_name }}</td>
            <td id="td2_style"> {{ $warehouse->cargo_price }}</td>
            <td id="td2_style"> {{ $warehouse->destination }}</td>
            <td id="td2_style"> {{ $warehouse->warehouse_id }}</td>
        </tr>
    </table>
    </div>
    <div class="container"  style="display: flex; justify-content: center; align-items: center">
    <button onclick="window.location.href = href='/FRC/public/warehouses'" id="back_button_style">Повернутись до списку</button>
    </div>
@endsection
