<html>
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Інформаційна система FRC</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<style type="text/css">
    #td1_style {
        font-family: ISOCPEUR;
        font-size: 20px;
        font-weight: bold;
        width: 120px;
        text-align: center;
        border: 2px solid crimson;
    }
    #td2_style {
        font-family: ISOCPEUR;
        font-size: 20px;
        font-weight: bold;
        padding-left: 55px;
        padding-right: 55px;
        height: 70px;
        text-align: center;
        border: 2px solid crimson;
    }
    #th_style {
        font-family: ISOCPEUR;
        font-size: 20px;
        height: 70px;
        padding-left: 55px;
        padding-right: 55px;
        font-weight: bold;
        text-align: center;
        border-left: 3px solid crimson;
        border-bottom: 4px solid crimson;
    }
    #td_style {
        font-family: ISOCPEUR;
        font-size: 20px;
        height: 70px;
        padding-left: 55px;
        padding-right: 55px;
        text-align: center;
        border: 2px solid crimson;
        border-left: 3px solid crimson;
    }
    #span_style {
        font-family: ISOCPEUR;
        font-size: 20px;
        font-weight: bold;
        margin: 7px;
    }
    #text_style1 {
        font-family: ISOCPEUR;
        font-size: 20px;
        color: black;
        font-weight: bold;
        margin: 7px;
    }

    #href_style {
        font-family: ISOCPEUR;
        font-size: 20px;
        color: crimson;
        font-weight: bold;
        margin: 7px;
        text-align: center;
    }
    #select_style {
        font-family: ISOCPEUR;
        font-size: 20px;
        border-radius: 5px;
        font-weight: bold;
        color: aliceblue;
        background: crimson;
        cursor: pointer;
        padding: 2px;
        box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    }
    #submit_style {
        font-family: ISOCPEUR;
        font-size: 20px;
        font-weight: bold;
        color: aliceblue;
        width: 225px;
        background: crimson;
        border-radius: 5px;
        cursor: pointer;
        padding: 3px;
        margin-top: 17px;
        box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    }
    #button_style1 {
        font-family: ISOCPEUR;
        font-size: 20px;
        font-weight: bold;
        color: aliceblue;
        width: 225px;
        background: crimson;
        border-radius: 5px;
        cursor: pointer;
        padding: 3px;
        margin-bottom: 17px;
        margin-top: 17px;
        box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    }
    #table_style {
        border: 4px solid crimson;
        padding: 3px;
        background-color: white;
    }
    #input_style {
        font-family: ISOCPEUR;
        font-size: 20px;
        font-weight: bold;
        width: 200px;
        text-align: center;
        border-radius: 3px;
    }
    #input_style2 {
        font-family: ISOCPEUR;
        font-size: 20px;
        font-weight: bold;
        color: aliceblue;
        width: 200px;
        background: crimson;
        border-radius: 5px;
        padding: 3px;
    }
    #back_button_style
    {
        font-family: ISOCPEUR;
        font-size: 20px;
        font-weight: bold;
        color: aliceblue;
        width: 250px;
        background: crimson;
        border-radius: 5px;
        cursor: pointer;
        margin-top: 17px ;
        padding: 3px;
        box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);

    }
</style>

<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        @if (Route::has('login'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Вхід') }}</a>
                            </li>
                        @endif

                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Зареєструватись') }}</a>
                            </li>
                        @endif
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Вихід') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    <main class="py-4">
        @yield('content')
    </main>
</div>
<div class="container"  style="display: flex; justify-content: center; align-items: center">
@include('app.layouts_frc.footer')
</div>
</body>
</html>
