@extends('admin.layout')
<style type="text/css">
    label {
        min-width: 150px;
        display: inline-block;
    }
</style>
@section('content')
    <div class="container"  style="display: flex; justify-content: center; align-items: center">
    <h2 id="text_style2">Додати вантаж</h2>
    </div>
    <div class="container"  style="display: flex; justify-content: center; align-items: center">
    <form action="/FRC/public/admin/warehouses" method="POST">
        {{ csrf_field() }}
        <input type="text" name="cargo_name" id="input_style" placeholder="назва вантажу">
        <br/><br/>
        <input type="text" name="cargo_price" id="input_style" placeholder="ціна вантажу">
        <br/><br/>
        <input type="text" name="destination" id="input_style" placeholder="місце призначення">
        <br/><br/>
        <label id="span_style">Назва складу: </label>
        <select name="warehouse_id" id="select_style">
            @foreach($warehouse_id as $type_id => $type_title)
                <option value="{{ $type_id }}">
                    {{ $type_title }}
                </option>
            @endforeach
        </select>
        <br/><br/>
        <input type="submit" value="Зберегти" id="input_style2">
    </form>
    </div>
@endsection
