@extends('admin.layout')
<style type="text/css">

</style>
<style type="text/css">
    label {
        min-width: 150px;
        display: inline-block;
    }
</style>
@section('content')
    <div class="container"  style="display: flex; justify-content: center; align-items: center">
    <h2 id="text_style2">Редагування вантажу</h2>
    </div>
    <div class="container"  style="display: flex; justify-content: center; align-items: center">
    <form action="/FRC/public/admin/warehouses/{{ $warehouse->cargo_id }}" method="POST">
        {{ method_field('PUT') }}
        {{ csrf_field() }}
        <input type="text" name="cargo_name" id="input_style" placeholder="назва вантажу" value="{{ $warehouse->cargo_name }}">
        <br/><br/>
        <input type="text" name="cargo_price" id="input_style" placeholder="ціна вантажу" value="{{ $warehouse->cargo_price }}">
        <br/><br/>
        <input type="text" name="destination" id="input_style" placeholder="місце призначення" value="{{ $warehouse->destination }}">
        <br/><br/>
        <label id="span_style">Назва складу: </label>
        <select name="warehouse_id" id="select_style">
            @foreach($warehouse_name as $type_id => $type_title)
                <option value="{{ $type_id }}">
                    {{ $type_title }}
                </option>
            @endforeach
        </select>
        <br/>
        <br/>
        <input type="submit" value="Зберегти" id="input_style2">
    </form>
    </div>
@endsection
