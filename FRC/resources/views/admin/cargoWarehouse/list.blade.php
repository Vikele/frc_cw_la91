@extends('admin.layout')

@section('content')

    <div class="container"  style="display: flex; justify-content: center; align-items: center">
    <p id="H_style1">Список вантажів</p>
    </div>
    <div class="container"  style="display: flex; justify-content: center; align-items: center">
    <table id="table_style">
        <th id="td2_style">Назва вантажу</th>
        <th id="td2_style">Ціна вантажу</th>
        <th id="td2_style">id вантажуи</th>
        <th id="td2_style">Назва складу</th>
        <th id="td2_style">Місце призначення</th>
        <th id="td2_style">Опції</th>
        @foreach ($cargos as $warehouse)
            <tr>
                <td id="td1_style">
                    <a href="/FRC/public/warehouses/{{ $warehouse->cargo_id }}" id="href_style">{{ $warehouse->cargo_name}}</a>
                </td>
                <td id="td1_style">{{ $warehouse->cargo_price }}</td>
                <td id="td1_style">{{ $warehouse->cargo_id }}</td>
                <td id="td1_style">{{ $warehouse->warehouse_name }}</td>
                <td id="td1_style">{{ $warehouse->destination }}</td>
                <td id="td1_style">
                    <a href="/FRC/public/admin/warehouses/{{ $warehouse->cargo_id }}/edit" id="href_style">редагувати</a>
                    <form style="margin:  5px;" action="/FRC/public/admin/warehouses/{{ $warehouse->cargo_id }}"method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button id="select_style">Видалити</button>

                    </form>
                </td>
            </tr>
        @endforeach
    </table>
    </div>

@endsection
