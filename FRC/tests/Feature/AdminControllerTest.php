<?php


namespace Tests\Feature;

use App\Http\Controllers\AdminController;
use App\Models\User;
use App\Models\Warehouses;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use phpDocumentor\Reflection\Types\ContextFactory;
use Tests\TestCase;
use Illuminate\Database\Eloquent\Factories\Factory;
use Database\Factories\UserFactory;
use App\Models\Cargo;

class AdminControllerTest extends TestCase
{

    /** @test  */

    public function TestLoggedInAdmin(){
        $response = $this->get('/admin/warehouses')
            ->assertRedirect('/login');
    }

    /** @test  */

    public function TestInAdmin(){
        $response = $this->get('/admin/warehouses')
            ->assertRedirect('/login');
    }

    /** @test  */

    public function TestAuthAdmin(){

        $user = Auth::loginUsingId(2);

        $this->actingAs($user);

        $response = $this->get('/admin/warehouses')
            ->assertOk();
    }

    /** @test  */

    public function TestAuthUser(){

        $user = Auth::loginUsingId(1);

        $this->actingAs($user);

        $response = $this->get('/admin/warehouses')
            ->assertOk();
    }

    /** @test  */

    public function TestPost(){
        Event::fake();

        $user = Auth::loginUsingId(2);

        $this->withoutMiddleware();
        $this->actingAs($user);

        $response = $this->post('admin/warehouses', [
            'cargo_name' => 'Нафта',
            'cargo_price' => '10000',
            'warehouse_id' => '1',
            'destination' => 'Берлін'

        ]);

        $this->assertCount(7, Cargo::all());


    }



}
