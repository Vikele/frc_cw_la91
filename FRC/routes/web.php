<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WarehouseController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ErrorController;
use App\Http\Controllers\UsersController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware('auth')->group(function (){

Route::get('warehouses', [WarehouseController::class, 'index']);
Route::get('warehouses/{warehouse}', [WarehouseController::class, 'warehouse']);

Route::resource('admin/warehouses', AdminController::class)->middleware('checkAdmin');
Route::resource('operations', UsersController::class)->middleware('checkAdmin');

Route::get('/error', [ErrorController::class, 'index'])->name('error');

});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
