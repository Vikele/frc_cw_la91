<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Warehouses //extends Model
{
    //use HasFactory;
    public static $warehouse_id = array(
        '1' => 'A',
        '2' => 'B',
        '3' => 'C'
    );

    public function getCargoID(){
        return DB::select('select distinct(cargo_id) from cargos order by cargo_id');
    }

    public function getWarehouses($wh_name, $cargo_id)
    {
        $query = DB::table('cargos');
        $query->select('warehouse_id', 'cargo_name', 'cargo_id', 'destination', 'cargo_price', 'warehouse_name')
            ->orderBy('warehouse_name');
        if ($wh_name) {
            $query->where('warehouse_id', '=', $wh_name);
        }
        if ($cargo_id) {
            $query->where('cargo_id', '=', $cargo_id);
        }
        $warehouses = $query->get();
        return $warehouses;
    }


    public function getWarehouseByID($id)
    {
        if(!$id) return null;
        $warehouses = DB::table('cargos')
            ->select('*')
            ->where('cargo_id', $id)
            ->get()->first();
        return $warehouses;
    }

}
