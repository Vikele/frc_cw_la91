<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cargo;
use App\Models\Warehouses;
use Illuminate\Support\Facades\Redirect;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cargos = Cargo::get();
        return view('admin.cargoWarehouse.list', ['cargos' => $cargos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.cargoWarehouse.add', ['warehouse_id' => Warehouses::$warehouse_id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cargo_name = $request->input('cargo_name');
        $cargo_price = $request->input('cargo_price');
        $warehouse_id = $request->input('warehouse_id');
        $destination = $request->input('destination');
        $warehouse = new Cargo();
        $warehouse->cargo_name = $cargo_name;
        $warehouse->warehouse_name = Warehouses::$warehouse_id[$warehouse_id];
        $warehouse->cargo_price = $cargo_price;
        $warehouse->warehouse_id = $warehouse_id;
        $warehouse->destination = $destination;
        $warehouse->save();
        return Redirect::to('/admin/warehouses');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $warehouse = Cargo::where('cargo_id', $id)->first();
        return view('admin.cargoWarehouse.edit', [
            'warehouse' => $warehouse,
            'warehouse_name' => Warehouses::$warehouse_id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $warehouse = Cargo::where('cargo_id', $id)->first();
        $warehouse->cargo_name = $request->input('cargo_name');
        $warehouse->cargo_price = $request->input('cargo_price');
        $warehouse->warehouse_id = $request->input('warehouse_id');
        $warehouse->destination = $request->input('destination');
        $warehouse->warehouse_name = $warehouse->warehouse_name = Warehouses::$warehouse_id[$warehouse->warehouse_id] ;
        $warehouse->save();
        return Redirect::to('/admin/warehouses');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cargo::destroy($id);
        return Redirect::to('/admin/warehouses');
    }
}
