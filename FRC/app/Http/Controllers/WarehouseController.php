<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Warehouses;
use Illuminate\Support\Facades\DB;

class WarehouseController extends Controller
{
    public function index(Request $request){
        $warehouse_id = $request->input('warehouse_id', null);
        $cargo_id = $request->input('cargo_id', null);

        $model_warehouses = new Warehouses();


        $warehouse = $model_warehouses->getWarehouses($warehouse_id, $cargo_id);

        return view('app.warehouses.list', [
                'warehouses' => $warehouse,
                'warehouse_id' => Warehouses::$warehouse_id,
                'warehouse_id_selected' => $warehouse_id,
                'cargo_id_selected' => $cargo_id,]

        );
    }

    public function warehouse($id){
        $model_warehouses = new Warehouses();
        $warehouse = $model_warehouses->getWarehouseByID($id);
        return view('app.warehouses.warehouse')->with('warehouse',  $warehouse);
    }

}
